//
//  secondViewController.m
//  gameTableDemo
//
//  Created by Ranosys on 7/14/16.
//  Copyright © 2016 Ranosys. All rights reserved.
//

#import "secondViewController.h"

@interface secondViewController ()<UIScrollViewDelegate>
@property (strong, nonatomic) IBOutlet UIView *mySecondView;
@property (strong, nonatomic) IBOutlet UIScrollView *scroll;
@property (strong, nonatomic) IBOutlet UIImageView *image1;
@property (strong, nonatomic) IBOutlet UIImageView *image2;
@property (strong, nonatomic) IBOutlet UIImageView *image3;
@property (strong, nonatomic) IBOutlet UINavigationItem *NavigationItem2;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *viewWidth;

@end

@implementation secondViewController{
}
    - (void)viewDidLoad {
    [super viewDidLoad];

    _gameDetail.text = _gameDesc ;
    _NavigationItem2.title = _gameTitle;
    _image1.image = _gameImage ;
    _image2.image = [UIImage imageNamed:[_gamePics objectAtIndex:0]];
    _image3.image = [ UIImage imageNamed:[_gamePics objectAtIndex:1]];
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        CGFloat screenWidth = screenRect.size.width;
        CGFloat screenHeight = screenRect.size.height;
        _viewWidth.constant = screenWidth * 3;
    _scroll.contentSize = CGSizeMake(screenWidth * 3 + 1, 172);
    self.automaticallyAdjustsScrollViewInsets = NO;
        NSLog(@"%f%f",screenWidth,screenHeight);
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
//    _label.text = _s_demo;
    // Dispose of any resources that can be recreated.
}
//- (void)scrollViewDidScroll:(UIScrollView *)aScrollView
//{
//    [aScrollView setContentOffset: CGPointMake(aScrollView.contentOffset.x,0)];
//    
//}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
