//
//  secondViewController.h
//  gameTableDemo
//
//  Created by Ranosys on 7/14/16.
//  Copyright © 2016 Ranosys. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface secondViewController : UIViewController
@property (strong, nonatomic) NSString * gameTitle;
@property (strong, nonatomic) IBOutlet UILabel * gameDetail;
@property (strong, nonatomic) UIImage * gameImage;
@property (strong, nonatomic) NSString * gameDesc;
@property (strong, nonatomic) NSMutableArray * gamePics;
@end
